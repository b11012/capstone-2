// DEPENDENCIES
const Product = require('../models/Product');
const bcrypt = require('bcryptjs');
const auth = require('../auth')

// ADDING PRODUCTS
module.exports.addProduct = (req, res) => {

	console.log(req.body);

	Product.findOne({name: req.body.name}).then(result => {

		console.log(result);

		if(result !== null && result.name === req.body.name){
			return res.send('Product is already available')

		} else {

			let addNewProduct = new Product({
				name: req.body.name,
				description: req.body.description,
				price: req.body.price
			})

			addNewProduct.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));
		}
	})
	.catch(error => res.send(error));
};


// RETRIEVE ALL PRODUCTS
module.exports.showAllProducts = (req, res) => {

	Product.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err))
};

// RETRIEVE SINGLE PRODUCT

module.exports.getSingleProductController = (req, res) => {

	console.log(req.params);

	Product.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
};

// UPDATE COURSE

module.exports.updateProduct = (req, res) => {

	let updates = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Product.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updateProduct => res.send(updateProduct))
	.catch(err => res.send(err))
}

// ARCHIVE PRODUCT (DEACTIVATE)

module.exports.deactivateProduct = (req, res) => {

	console.log(req.user.id);

	console.log(req.params.id);

	let updates = {

		isActive: false
	}

	Product.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(deactivatedProduct => res.send(deactivatedProduct))
	.catch(err => res.send(err))
}

// ACTIVATE PRODUCT

module.exports.activateProduct = (req, res) => {

	console.log(req.user.id);

	console.log(req.params.id);

	let updates = {

		isActive: true
	}

	Product.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(activatedProduct => res.send(activatedProduct))
	.catch(err => res.send(err))
}

// RETRIEVE ALL ACTIVE PRODUCTS

module.exports.retrieveActiveProducts = (req, res) => {

	console.log(req.body)

	Product.find({isActive: true})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// DELETE PRODUCTS
module.exports.deleteProduct = (req, res) => {

	console.log(req.params);

	Product.findByIdAndDelete(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

