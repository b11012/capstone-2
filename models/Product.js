/*
Product
Name - string,
Description - string,
Price - number,
isActive - boolean, defaults to true,
createdOn - Date, defualts to current date
*/

const mongoose = require('mongoose');

let productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Product name is required"]
	},

	description: {
		type: String,
		required: [true, "Description is required"]
	},

	price: {
		type: Number,
		required: [true, "Price is required"]
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date()
	}

	// purchaseBy: [
	// 	{
	// 		userId:{
	// 			type: String,
	// 			required: [true, "User Id is required"]
	// 		}

	// 		status: {
	// 			type: String,
	// 			default: "Wew"
	// 		}
	// 	}
	// ]
})

module.exports = mongoose.model("Product", productSchema);