/*
User
email - string,
password - string,
isAdmin (Boolean - defaults to false)
*/

const mongoose = require('mongoose');
let userSchema = new mongoose.Schema({

	email: {
		type: String,
		required: [true, "Please type in your email"]
	},

	password: {
		type: String,
		required: [true, "Please type in your password"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	order: [
		{
			productId: {
				type: String,
				required: [true, "Product Id is required"]
			},
				
			totalAmount: {
				type: Number,
				required: [true, "Total amount is required"]
			},

			purchasedOn: {
				type: Date,
				default: new Date()
			}
					
		}
	]
})

module.exports = mongoose.model("User", userSchema);

