// DEPENDENCIES
const express = require('express');
const router = express.Router();

// IMPORTED MODULES
const productControllers = require('../controllers/productControllers');

const auth = require('../auth');

const {verify, verifyAdmin} = auth;

// ROUTES

// ADDING PRODUCT
router.post('/', verify, verifyAdmin, productControllers.addProduct);

// RETRIEVE ALL PRODUCTS
router.get('/', productControllers.showAllProducts)

// RETRIEVE SINGLE PRODUCT
router.get('/getSingleProduct/:id', productControllers.getSingleProductController)

// UPDATE A PRODUCT
router.put('/updateProduct/:id', verify, verifyAdmin, productControllers.updateProduct)

// DEACTIVATING A PRODUCT
router.put('/archive/:id', verify, verifyAdmin, productControllers.deactivateProduct)

// ACTIVATING A PRODUCT
router.put('/activate/:id', verify, verifyAdmin, productControllers.activateProduct)

// RETRIEVE ACTIVE PRODUCTS
router.get('/getActiveProducts', productControllers.retrieveActiveProducts)

// DELETE PRODUCT
router.delete('/deleteProduct/:id',verify, verifyAdmin, productControllers.deleteProduct)
module.exports = router;


