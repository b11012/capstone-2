// DEPENDENCIES
const express = require('express');
const router = express.Router();

// IMPORTED MODULES
const userControllers = require('../controllers/userControllers');
const auth = require('../auth')

// auth
const {verify, verifyAdmin} = auth;

// ROUTES

// User registration
router.post('/', userControllers.userRegistration);

// show all users
router.get('/', userControllers.showAllUsers);

// login user
router.post('/login', userControllers.userLogin)

// get user detail by bearer token
router.get('/getUserTokenDetails', verify, userControllers.userTokenDetails)

// get user detail by id
router.get('/getUserDetails/:id', userControllers.getSingleUserController)

// Update isAdmin details to true
router.put('/updateTurnAdmin/:id', verify, userControllers.updateTurnAdmin)

// Update isAdmin details to false
router.put('/updateFalseAdmin/:id', verify, userControllers.falseAdmin)

// Order User
router.post('/order', verify, userControllers.ordered);

// Get Order
router.get('/getOrder', verify, userControllers.getOrder)


module.exports = router;